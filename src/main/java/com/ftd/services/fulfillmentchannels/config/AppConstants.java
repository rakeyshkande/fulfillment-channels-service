package com.ftd.services.fulfillmentchannels.config;

public final class AppConstants {

    public static final String SAMEDAY_ORDER = "SameDayOrder";
    public static final String NEXTDAY_ORDER = "NextDayOrder";
    public static final String FUTUREDAY_ORDER = "FutureDayOrder";

    public static final String ACTIVE = "Active";
    public static final String INACTIVE = "Inactive";
    public static final String[] DAYS = {"sun", "mon", "tue", "wed", "thu", "fri", "sat"};

    public static final Long DAY_FACTOR = 86400000L;

    public static final Integer EXE_SERV_BATCH_SIZE = 30;
}
