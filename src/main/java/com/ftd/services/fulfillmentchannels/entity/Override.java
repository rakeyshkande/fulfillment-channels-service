package com.ftd.services.fulfillmentchannels.entity;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Override {

    private String level;
    private Date startDate;
    private Date endDate;
    private String siteId;
    private String shipType;
    private String shipService;
    private String dayOfWeek;
    private String openTimes;
    private String deliveryTimes;
    private List<String> skus;
    private List<String> products;
    private List<String> productGroups;
    private List<Date> holiday;

}
