package com.ftd.services.fulfillmentchannels.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MemberProfile {

    private String brandId;
    private String memberNo;
    private String memberCode;
    private String memberName;
    private String memberType;
    private String memberUniqueRef;
    private String status;
    private String erosStatus;
    private String apolloStatus;
    private String timeZone;
    private String xCoordinate;
    private String yCoordinate;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String originZipCode;
    private String country;
    private Boolean sendOnly;
    private String orderItemMinima;
    private Boolean holidayException;
    private Boolean acceptFutureOrders;
    private OperationalHourInfo operationalHourInfo;

}
