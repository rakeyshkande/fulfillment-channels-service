package com.ftd.services.fulfillmentchannels.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductDetails {

    private String code;
    private Float price;
    private String prepTime;
    private String transTime;
    private Dimensions dimensions;
}
