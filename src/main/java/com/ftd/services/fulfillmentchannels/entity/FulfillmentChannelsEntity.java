package com.ftd.services.fulfillmentchannels.entity;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
@Document(collection = "fulfillmentChannelInfoSub")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FulfillmentChannelsEntity extends AuditDetails {

    @Id
    private String fulfillmentChannelsId;
    private String siteGroup;
    private String deliveryZipCode;
    private String locationCode;
    private String channel;
    private List<String> cities;
    private String state;
    private String country;
    private List<String> productGroups;
    private List<String> skus;
    private MemberProfile member;
    private List<FulfillmentChannel> fulfilmentOptions;
    private List<Override> overrides;
}
