package com.ftd.services.fulfillmentchannels.entity;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
@Document(collection = "productGroupsSub")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductGroup extends AuditDetails {

    @Id
    private String id;
    private String groupName;
    private List<ProductDetails> products;
}
