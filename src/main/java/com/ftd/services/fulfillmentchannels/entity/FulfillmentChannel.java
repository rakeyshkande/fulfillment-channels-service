package com.ftd.services.fulfillmentchannels.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FulfillmentChannel {

    private String type;
    private String service;
    private String serviceType;
    private List<String> channels;
    private String leadTime;
    private String carrierId;
    private String carrierName;
    private String timeInTransit;
    private Dimensions dimensions;
    private List<String> productGroups;
    private List<String> skus;
    private CutoffTimes cutoffTimes;

}
