package com.ftd.services.fulfillmentchannels.service.impl;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.AbstractMap.SimpleEntry;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.ftd.services.fulfillmentchannels.config.AppConstants;
import com.ftd.services.fulfillmentchannels.domain.api.request.DeliveryAPIRequest;
import com.ftd.services.fulfillmentchannels.domain.api.response.DeliveryAPIResponse;
import com.ftd.services.fulfillmentchannels.domain.internal.DeliveryAPITempOut;
import com.ftd.services.fulfillmentchannels.entity.FulfillmentChannelsEntity;
import com.ftd.services.fulfillmentchannels.entity.OperationHours;
import com.ftd.services.fulfillmentchannels.entity.ProductGroup;
import com.ftd.services.fulfillmentchannels.entity.Dimensions;
import com.ftd.services.fulfillmentchannels.entity.FulfillmentChannel;

@Service
public class BaseAPIServiceImpl {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    protected String getHoursAsString(OperationHours openHours, String dayOfWeek) {
        String hours = null;
        switch (dayOfWeek) {
        case "sun":
            hours = openHours.getSun();
            break;
        case "mon":
            hours = openHours.getMon();
            break;
        case "tue":
            hours = openHours.getTue();
            break;
        case "wed":
            hours = openHours.getWed();
            break;
        case "thu":
            hours = openHours.getThu();
            break;
        case "fri":
            hours = openHours.getFri();
            break;
        case "sat":
            hours = openHours.getSat();
            break;
        default:
            break;
        }
        return hours;
    }

    protected List<DeliveryAPIResponse> getDeliveryAvailability(List<FulfillmentChannelsEntity> channels,
            DeliveryAPIRequest req, ProductGroup productGroups, LocalDateTime orderingTime) {
        final Integer dim = 10;
        Dimensions inputProductDimentions = new Dimensions(dim, dim, dim, dim);
        LocalDateTime inputDeliveryDate = req.getDeliveryDateTime().atZone(ZoneId.systemDefault()).toLocalDateTime()
                .withHour(0).withMinute(0).withSecond(0).withNano(0);

        List<DeliveryAPITempOut> intermList = new ArrayList<>();
        for (int i = 0; i < channels.size(); i++) {
            FulfillmentChannelsEntity fEntity = channels.get(i);
            DeliveryAPITempOut interm = new DeliveryAPITempOut();
            interm.setMemberNo(fEntity.getMember().getMemberNo());
            String orderingDayType = "";
            orderingTime = orderingTime.atZone(ZoneId.systemDefault())
                    .withZoneSameInstant(TimeZone.getTimeZone(fEntity.getMember().getTimeZone()).toZoneId())
                    .toLocalDateTime();
            long diffInDays = Period.between(orderingTime.toLocalDate(), inputDeliveryDate.toLocalDate()).getDays();
            if (diffInDays == 0) {
                orderingDayType = AppConstants.SAMEDAY_ORDER;
            } else if (diffInDays == 1) {
                orderingDayType = AppConstants.NEXTDAY_ORDER;
            } else if (diffInDays > 1) {
                orderingDayType = AppConstants.FUTUREDAY_ORDER;
            } else {
                continue;
            }
            if (AppConstants.ACTIVE.equalsIgnoreCase(fEntity.getMember().getStatus())
                    && AppConstants.ACTIVE.equalsIgnoreCase(fEntity.getMember().getErosStatus())
                    && AppConstants.ACTIVE.equalsIgnoreCase(fEntity.getMember().getApolloStatus())
                    || fEntity.getMember().getSendOnly()
                    // || inputOrderItemValue >
                    // Double.parseDouble(fEntity.getMember().getOrderItemMinima())
                    // || !fEntity.getMember().getAcceptFutureOrders()
                    || inputDeliveryDate.toLocalDate().isBefore(orderingTime.toLocalDate())) {
                continue;
            }
            String openHoursStr = this.getHoursAsString(fEntity.getMember().getOperationalHourInfo().getOpenHours(),
                    inputDeliveryDate.getDayOfWeek().getDisplayName(TextStyle.SHORT_STANDALONE, Locale.getDefault())
                            .toLowerCase());
            String deliveryHoursStr = this.getHoursAsString(
                    fEntity.getMember().getOperationalHourInfo().getDeliveryHours(), inputDeliveryDate.getDayOfWeek()
                            .getDisplayName(TextStyle.SHORT_STANDALONE, Locale.getDefault()).toLowerCase());
            deliveryHoursStr = ObjectUtils.isEmpty(deliveryHoursStr) ? openHoursStr : deliveryHoursStr;
            LocalDateTime shopOpenTimeCal = null;
            LocalDateTime shopCloseTimeCal = null;
            LocalDateTime deliveryStartTimeCal = null;
            LocalDateTime deliveryEndTimeCal = null;
            if (!ObjectUtils.isEmpty(openHoursStr)) {
                String[] openHours = openHoursStr.split("-");
                shopOpenTimeCal = LocalDateTime.parse(inputDeliveryDate.toLocalDate() + "T" + openHours[0]);
                shopCloseTimeCal = LocalDateTime.parse(inputDeliveryDate.toLocalDate() + "T" + openHours[1]);
            } else {
                continue;
            }
            if (!ObjectUtils.isEmpty(deliveryHoursStr)) {
                String[] deliveryHours = deliveryHoursStr.split("-");
                deliveryStartTimeCal = LocalDateTime.parse(inputDeliveryDate.toLocalDate() + "T" + deliveryHours[0]);
                deliveryEndTimeCal = LocalDateTime.parse(inputDeliveryDate.toLocalDate() + "T" + deliveryHours[1]);
            }
            List<String> fulfillmentOptions = new ArrayList<>();
            if (!ObjectUtils.isEmpty(fEntity.getFulfilmentOptions())) {
                for (int j = 0; j < fEntity.getFulfilmentOptions().size(); j++) {
                    LocalDateTime shopOpenTime = shopOpenTimeCal;
                    LocalDateTime shopCloseTime = shopCloseTimeCal;
                    LocalDateTime deliveryStartTime = deliveryStartTimeCal;
                    LocalDateTime deliveryEndTime = deliveryEndTimeCal;
                    FulfillmentChannel option = fEntity.getFulfilmentOptions().get(j);
                    Duration orderCreationAge = null;
                    Duration inputProductPrepTime = Duration.parse(productGroups.getProducts().stream()
                            .map(p -> p.getPrepTime()).sorted().findFirst().orElse("PT00H00M00S"));
                    Duration inputProductTransitTime = Duration.parse(productGroups.getProducts().stream()
                            .map(p -> p.getTransTime()).sorted().findFirst().orElse("PT00H00M00S"));
                    if (ObjectUtils.isEmpty(option.getLeadTime())) {
                        orderCreationAge = inputProductPrepTime;
                    } else {
                        orderCreationAge = inputProductPrepTime.plus(
                                Duration.parse(dateFormat.format(inputDeliveryDate) + " " + option.getLeadTime()));
                    }

                    if (!ObjectUtils.isEmpty(inputProductTransitTime) && !ObjectUtils.isEmpty(option.getTimeInTransit())
                            && inputProductTransitTime.compareTo(Duration
                                    .parse(dateFormat.format(inputDeliveryDate) + " " + option.getTimeInTransit())) > 0
                            || !option.getProductGroups().contains(productGroups.getGroupName())
                            || !ObjectUtils.isEmpty(option.getDimensions())
                                    && !ObjectUtils.isEmpty(inputProductDimentions)
                                    && (option.getDimensions().getLength() < inputProductDimentions.getLength()
                                            || option.getDimensions().getWidth() < inputProductDimentions.getWidth()
                                            || option.getDimensions().getHeight() < inputProductDimentions.getHeight()
                                            || option.getDimensions().getWeight() < inputProductDimentions
                                                    .getWeight())) {
                        continue;
                    }
                    String serviceCutoffStr = this.getHoursAsString(option.getCutoffTimes().getLocal(),
                            inputDeliveryDate.getDayOfWeek()
                                    .getDisplayName(TextStyle.SHORT_STANDALONE, Locale.getDefault()).toLowerCase());
                    LocalDateTime serviceCutoffStart = null;
                    LocalDateTime serviceCutoffEnd = null;
                    if (!ObjectUtils.isEmpty(serviceCutoffStr)) {
                        String[] serviceCutoffs = serviceCutoffStr.split("-");
                        if (serviceCutoffs.length > 1) {
                            serviceCutoffStart = LocalDateTime
                                    .parse(inputDeliveryDate.toLocalDate() + "T" + serviceCutoffs[0]);
                            serviceCutoffEnd = LocalDateTime
                                    .parse(inputDeliveryDate.toLocalDate() + "T" + serviceCutoffs[1]);
                        } else {
                            serviceCutoffStart = LocalDateTime
                                    .parse(inputDeliveryDate.toLocalDate() + "T" + "00:00:00");
                            serviceCutoffEnd = LocalDateTime
                                    .parse(inputDeliveryDate.toLocalDate() + "T" + serviceCutoffs[0]);
                        }
                    }
                    LocalDateTime shopOpenTime0 = null;
                    LocalDateTime shopCloseTime0 = null;
                    if (AppConstants.NEXTDAY_ORDER.equalsIgnoreCase(orderingDayType)) {
                        String openHoursStr0 = this
                                .getHoursAsString(fEntity.getMember().getOperationalHourInfo().getOpenHours(),
                                        inputDeliveryDate.minusDays(1).getDayOfWeek()
                                                .getDisplayName(TextStyle.SHORT_STANDALONE, Locale.getDefault())
                                                .toLowerCase());
                        if (!ObjectUtils.isEmpty(openHoursStr0)) {
                            String[] openHours0 = openHoursStr0.split("-");
                            shopOpenTime0 = LocalDateTime.parse(inputDeliveryDate.toLocalDate() + "T" + openHours0[0]);
                            shopCloseTime0 = LocalDateTime.parse(inputDeliveryDate.toLocalDate() + "T" + openHours0[1]);
                        } else {
                            shopOpenTime0 = LocalDateTime.parse(inputDeliveryDate.toLocalDate() + "T" + "00:00:00");
                            shopCloseTime0 = LocalDateTime.parse(inputDeliveryDate.toLocalDate() + "T" + "00:00:00");
                        }
                    }
                    Duration shopDuration;
                    Duration deliveryDuration;
                    if ("SUNDAY_DELIVERY".equalsIgnoreCase(option.getService()) && "sun" != inputDeliveryDate
                            .getDayOfWeek().getDisplayName(TextStyle.SHORT_STANDALONE, Locale.getDefault())) {
                        continue;
                    }
                    if ("SAMEDAY_DELIVERY".equalsIgnoreCase(option.getService())
                            && (AppConstants.NEXTDAY_ORDER.equalsIgnoreCase(orderingDayType)
                                    || AppConstants.FUTUREDAY_ORDER.equalsIgnoreCase(orderingDayType))) {
                        continue;
                    }
                    if ("TIMED".equalsIgnoreCase(option.getServiceType())
                            || "SLOTTED".equalsIgnoreCase(option.getServiceType())) {
                        if (serviceCutoffStart.isAfter(deliveryEndTime)) {
                            continue;
                        }
                        if (serviceCutoffStart.isAfter(deliveryStartTime)) {
                            deliveryStartTime = serviceCutoffStart;
                        }
                        if (deliveryEndTime.isAfter(serviceCutoffEnd)) {
                            deliveryEndTime = serviceCutoffEnd;
                        }
                    }
                    if (!AppConstants.SAMEDAY_ORDER.equalsIgnoreCase(orderingDayType)
                            && "EXPRESS".equalsIgnoreCase(option.getServiceType())) {
                        continue;
                    }
                    if (AppConstants.SAMEDAY_ORDER.equalsIgnoreCase(orderingDayType)) {
                        if ("EXPRESS".equalsIgnoreCase(option.getServiceType())) {
                            serviceCutoffEnd = orderingTime
                                    .plus(Duration.between(serviceCutoffStart, serviceCutoffEnd));
                            if (!deliveryStartTime.isAfter(orderingTime)) {
                                deliveryStartTime = orderingTime.plus(inputProductPrepTime);
                            }
                        }
                        if (orderingTime.isAfter(shopCloseTime) || orderingTime.isAfter(deliveryEndTime)
                                || orderingTime.isAfter(serviceCutoffEnd)) {
                            continue;
                        }
                        if (shopOpenTime.isAfter(orderingTime)) {
                            shopDuration = Duration.between(shopOpenTime, shopCloseTime);
                            if (serviceCutoffEnd.isAfter(deliveryEndTime)) {
                                deliveryDuration = Duration.between(deliveryStartTime, deliveryEndTime);
                            } else {
                                deliveryDuration = Duration.between(deliveryStartTime, serviceCutoffEnd);
                            }
                        } else {
                            shopDuration = Duration.between(orderingTime, shopCloseTime);
                            if (deliveryStartTime.isAfter(orderingTime)) {
                                if (serviceCutoffEnd.isAfter(deliveryEndTime)) {
                                    deliveryDuration = Duration.between(deliveryStartTime, deliveryEndTime);
                                } else {
                                    deliveryDuration = Duration.between(deliveryStartTime, serviceCutoffEnd);
                                }
                            } else {
                                if (serviceCutoffEnd.isAfter(deliveryEndTime)) {
                                    deliveryDuration = Duration.between(orderingTime, deliveryEndTime);
                                } else {
                                    deliveryDuration = Duration.between(orderingTime, serviceCutoffEnd);
                                }
                            }
                        }
                    } else if (AppConstants.NEXTDAY_ORDER.equalsIgnoreCase(orderingDayType)) {
                        if (shopOpenTime0.isAfter(orderingTime)) {
                            shopDuration = Duration.between(shopOpenTime0, shopCloseTime0)
                                    .plus(Duration.between(shopOpenTime, shopCloseTime));
                        } else {
                            if (orderingTime.isAfter(shopCloseTime0)) {
                                shopDuration = Duration.between(shopOpenTime, shopCloseTime);
                            } else {
                                shopDuration = Duration.between(shopCloseTime0, orderingTime)
                                        .plus(Duration.between(shopOpenTime, shopCloseTime));
                            }
                        }
                        if (serviceCutoffEnd.isAfter(deliveryEndTime)) {
                            deliveryDuration = Duration.between(deliveryStartTime, deliveryEndTime);
                        } else {
                            deliveryDuration = Duration.between(deliveryStartTime, serviceCutoffEnd);
                        }
                    } else {
                        shopDuration = Duration.between(shopOpenTime, shopCloseTime);
                        if (serviceCutoffEnd.isAfter(deliveryEndTime)) {
                            deliveryDuration = Duration.between(deliveryStartTime, deliveryEndTime);
                        } else {
                            deliveryDuration = Duration.between(deliveryStartTime, serviceCutoffEnd);
                        }
                    }
                    if (inputProductPrepTime.compareTo(shopDuration) > 0
                            || orderCreationAge.compareTo(deliveryDuration) > 0) {
                        continue;
                    }
                    fulfillmentOptions.add(option.getService());
                }
            }
            if (ObjectUtils.isEmpty(fulfillmentOptions.size())) {
                continue;
            }
            interm.setFulfillmentOptions(fulfillmentOptions);
            intermList.add(interm);
        }
        if (!ObjectUtils.isEmpty(intermList)) {
            Map<String, List<String>> outList = new HashMap<>();
            intermList.parallelStream().forEach(l -> l.getFulfillmentOptions().forEach(k -> {
                List<String> stringList = null;
                if (outList.containsKey(k)) {
                    stringList = outList.get(k);
                } else {
                    stringList = new ArrayList<>();
                }
                stringList.add(l.getMemberNo());
                outList.put(k, stringList);
            }));
            List<DeliveryAPIResponse> l = new ArrayList<>();
            outList.forEach((k, v) -> l.add(new DeliveryAPIResponse(k, v)));
            return l;
        }
        return null;
    }

    protected SimpleEntry<String, List<DeliveryAPIResponse>> getDayCalendarAvailability(
            List<FulfillmentChannelsEntity> channels, String dayOfWeek, ProductGroup productGroups,
            LocalDateTime orderingTime) {
        final Integer dim = 10;
        Dimensions inputProductDimentions = new Dimensions(dim, dim, dim, dim);
        List<DeliveryAPITempOut> intermList = new ArrayList<>();
        for (int i = 0; i < channels.size(); i++) {
            FulfillmentChannelsEntity fEntity = channels.get(i);
            DeliveryAPITempOut interm = new DeliveryAPITempOut();
            interm.setMemberNo(fEntity.getMember().getMemberNo());
            if (AppConstants.ACTIVE.equalsIgnoreCase(fEntity.getMember().getStatus())
                    && AppConstants.ACTIVE.equalsIgnoreCase(fEntity.getMember().getErosStatus())
                    && AppConstants.ACTIVE.equalsIgnoreCase(fEntity.getMember().getApolloStatus())
                    || fEntity.getMember().getSendOnly()
            /*
             * || inputOrderItemValue >
             * Double.parseDouble(fEntity.getMember().getOrderItemMinima()) ||
             * !fEntity.getMember().getAcceptFutureOrders()
             */) {
                continue;
            }
            String openHoursStr = this.getHoursAsString(fEntity.getMember().getOperationalHourInfo().getOpenHours(),
                    dayOfWeek);
            String deliveryHoursStr = this
                    .getHoursAsString(fEntity.getMember().getOperationalHourInfo().getDeliveryHours(), dayOfWeek);
            deliveryHoursStr = ObjectUtils.isEmpty(deliveryHoursStr) ? openHoursStr : deliveryHoursStr;
            LocalDateTime shopOpenTimeCal = null;
            LocalDateTime shopCloseTimeCal = null;
            LocalDateTime deliveryStartTimeCal = null;
            LocalDateTime deliveryEndTimeCal = null;
            if (!ObjectUtils.isEmpty(openHoursStr)) {
                String[] openHours = openHoursStr.split("-");
                shopOpenTimeCal = LocalDateTime.parse(orderingTime.toLocalDate() + "T" + openHours[0]);
                shopCloseTimeCal = LocalDateTime.parse(orderingTime.toLocalDate() + "T" + openHours[1]);
            } else {
                continue;
            }
            if (!ObjectUtils.isEmpty(deliveryHoursStr)) {
                String[] deliveryHours = deliveryHoursStr.split("-");
                deliveryStartTimeCal = LocalDateTime.parse(orderingTime.toLocalDate() + "T" + deliveryHours[0]);
                deliveryEndTimeCal = LocalDateTime.parse(orderingTime.toLocalDate() + "T" + deliveryHours[1]);
            }
            List<String> fulfillmentOptions = new ArrayList<>();
            if (!ObjectUtils.isEmpty(fEntity.getFulfilmentOptions())) {
                for (int j = 0; j < fEntity.getFulfilmentOptions().size(); j++) {
                    LocalDateTime shopOpenTime = shopOpenTimeCal;
                    LocalDateTime shopCloseTime = shopCloseTimeCal;
                    LocalDateTime deliveryStartTime = deliveryStartTimeCal;
                    LocalDateTime deliveryEndTime = deliveryEndTimeCal;
                    FulfillmentChannel option = fEntity.getFulfilmentOptions().get(j);
                    Duration orderCreationAge = null;
                    Duration inputProductPrepTime = Duration.parse(productGroups.getProducts().stream()
                            .map(p -> p.getPrepTime()).sorted().findFirst().orElse("PT00H00M00S"));
                    Duration inputProductTransitTime = Duration.parse(productGroups.getProducts().stream()
                            .map(p -> p.getTransTime()).sorted().findFirst().orElse("PT00H00M00S"));
                    if (ObjectUtils.isEmpty(option.getLeadTime())) {
                        orderCreationAge = inputProductPrepTime;
                    } else {
                        orderCreationAge = inputProductPrepTime
                                .plus(Duration.parse(dateFormat.format(orderingTime) + " " + option.getLeadTime()));
                    }

                    if (!ObjectUtils.isEmpty(inputProductTransitTime) && !ObjectUtils.isEmpty(option.getTimeInTransit())
                            && inputProductTransitTime.compareTo(Duration
                                    .parse(dateFormat.format(orderingTime) + " " + option.getTimeInTransit())) > 0
                            || !option.getProductGroups().contains(productGroups.getGroupName())
                            || !ObjectUtils.isEmpty(option.getDimensions())
                                    && !ObjectUtils.isEmpty(inputProductDimentions)
                                    && (option.getDimensions().getLength() < inputProductDimentions.getLength()
                                            || option.getDimensions().getWidth() < inputProductDimentions.getWidth()
                                            || option.getDimensions().getHeight() < inputProductDimentions.getHeight()
                                            || option.getDimensions().getWeight() < inputProductDimentions
                                                    .getWeight())) {
                        continue;
                    }
                    String serviceCutoffStr = this.getHoursAsString(option.getCutoffTimes().getLocal(), dayOfWeek);
                    LocalDateTime serviceCutoffStart = LocalDateTime
                            .parse(orderingTime.toLocalDate() + "T" + "00:00:00");
                    LocalDateTime serviceCutoffEnd = LocalDateTime.parse(orderingTime.toLocalDate() + "T" + "00:00:00");
                    if (!ObjectUtils.isEmpty(serviceCutoffStr)) {
                        String[] serviceCutoffs = serviceCutoffStr.split("-");
                        if (serviceCutoffs.length > 1) {
                            serviceCutoffStart = LocalDateTime
                                    .parse(orderingTime.toLocalDate() + "T" + serviceCutoffs[0]);
                            serviceCutoffEnd = LocalDateTime
                                    .parse(orderingTime.toLocalDate() + "T" + serviceCutoffs[1]);
                        } else {
                            serviceCutoffEnd = LocalDateTime
                                    .parse(orderingTime.toLocalDate() + "T" + serviceCutoffs[0]);
                        }
                    }
                    Duration shopDuration;
                    Duration deliveryDuration;
                    if ("TIMED".equalsIgnoreCase(option.getServiceType())
                            || "SLOTTED".equalsIgnoreCase(option.getServiceType())) {
                        if (serviceCutoffStart.isAfter(deliveryStartTime)) {
                            deliveryStartTime = serviceCutoffStart;
                        }
                        if (deliveryEndTime.isAfter(serviceCutoffEnd)) {
                            deliveryEndTime = serviceCutoffEnd;
                        }
                    }
                    shopDuration = Duration.between(shopOpenTime, shopCloseTime);
                    if (serviceCutoffEnd.isAfter(deliveryEndTime)) {
                        deliveryDuration = Duration.between(deliveryStartTime, deliveryEndTime);
                    } else {
                        deliveryDuration = Duration.between(deliveryStartTime, serviceCutoffEnd);
                    }
                    if (inputProductPrepTime.compareTo(shopDuration) > 0
                            || orderCreationAge.compareTo(deliveryDuration) > 0) {
                        continue;
                    }
                    fulfillmentOptions.add(option.getService());
                }
            }
            if (ObjectUtils.isEmpty(fulfillmentOptions.size())) {
                continue;
            }
            interm.setFulfillmentOptions(fulfillmentOptions);
            intermList.add(interm);
        }

        if (!ObjectUtils.isEmpty(intermList)) {
            Map<String, List<String>> outList = new HashMap<>();
            for (DeliveryAPITempOut out1 : intermList) {
                for (String option : out1.getFulfillmentOptions()) {
                    List<String> value = null;
                    if (outList.containsKey(option)) {
                        value = outList.get(option);
                    } else {
                        value = new ArrayList<>();
                    }
                    value.add(out1.getMemberNo());
                    outList.put(option, value);
                }
            }
            List<DeliveryAPIResponse> l = new ArrayList<>();
            outList.forEach((k, v) -> l.add(new DeliveryAPIResponse(k, v)));
            return new AbstractMap.SimpleEntry<>(dayOfWeek, l);
        }
        return null;
    }

    protected SimpleEntry<LocalDate, List<DeliveryAPIResponse>> getDayOptions(LocalDate delDate,
            Map<String, List<DeliveryAPITempOut>> dayOfWeekList) {
        List<DeliveryAPITempOut> intermList = dayOfWeekList.get(
                delDate.getDayOfWeek().getDisplayName(TextStyle.SHORT_STANDALONE, Locale.getDefault()).toLowerCase());
        if (!ObjectUtils.isEmpty(intermList)) {
            Map<String, List<String>> outList = new HashMap<>();
            for (DeliveryAPITempOut out1 : intermList) {
                for (String option : out1.getFulfillmentOptions()) {
                    List<String> value = null;
                    if (outList.containsKey(option)) {
                        value = outList.get(option);
                    } else {
                        value = new ArrayList<>();
                    }
                    value.add(out1.getMemberNo());
                    outList.put(option, value);
                }
            }
            List<DeliveryAPIResponse> l = new ArrayList<>();
            outList.forEach((k, v) -> l.add(new DeliveryAPIResponse(k, v)));
            return new AbstractMap.SimpleEntry<>(delDate, l);
        }
        return null;
    }
}
