package com.ftd.services.fulfillmentchannels.service;

import java.util.List;

import com.ftd.services.fulfillmentchannels.domain.api.request.DeliveryAPIRequest;
import com.ftd.services.fulfillmentchannels.domain.api.response.DeliveryAPIResponse;
import com.ftd.services.fulfillmentchannels.domain.internal.enums.SiteId;


public interface DeliveryAPIService {

    List<DeliveryAPIResponse> getDeliveryAPIFulfillmentChannels(SiteId siteId, DeliveryAPIRequest req);

}
