package com.ftd.services.fulfillmentchannels.service.impl;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.ftd.commons.misc.exception.NoRecordsFoundException;
import com.ftd.services.fulfillmentchannels.dao.DeliveryAPIDao;
import com.ftd.services.fulfillmentchannels.domain.api.request.DeliveryAPIRequest;
import com.ftd.services.fulfillmentchannels.domain.api.response.DeliveryAPIResponse;
import com.ftd.services.fulfillmentchannels.domain.internal.enums.SiteId;
import com.ftd.services.fulfillmentchannels.entity.FulfillmentChannelsEntity;
import com.ftd.services.fulfillmentchannels.entity.ProductGroup;
import com.ftd.services.fulfillmentchannels.service.DeliveryAPIService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DeliveryAPIServiceImpl extends BaseAPIServiceImpl implements DeliveryAPIService {

    @Autowired
    private DeliveryAPIDao dao;

    @Override
    public List<DeliveryAPIResponse> getDeliveryAPIFulfillmentChannels(SiteId siteId, DeliveryAPIRequest req) {
        log.info("Invoked FulfillmentChannelServiceSubbuServiceImpl::getFulfillmentChannelsWithLocalDate");
        final LocalDateTime orderingTime = LocalDateTime.now().atZone(ZoneId.systemDefault()).toLocalDateTime();
        Optional<ProductGroup> productGroups = dao.getProductGroup(siteId, req.getProductIds());
        if (!productGroups.isPresent()) {
            throw new NoRecordsFoundException(null, null, HttpStatus.NOT_FOUND);
        } else {
            req.setProductGroup(productGroups.get().getGroupName());
        }
        Optional<List<FulfillmentChannelsEntity>> channelsOpt = dao.getDeliveryFulfillmentChannels(siteId,
                req);

        if (channelsOpt.isPresent()) {
            return this.getDeliveryAvailability(channelsOpt.get(), req, productGroups.get(), orderingTime);
        } else {
            throw new NoRecordsFoundException("RESOURCE_NOT_FOUND",
                    "No records found for the given site id - " + siteId, HttpStatus.NOT_FOUND);
        }
    }

}
