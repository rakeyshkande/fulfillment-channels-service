package com.ftd.services.fulfillmentchannels.service.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.ftd.commons.misc.exception.NoRecordsFoundException;
import com.ftd.services.fulfillmentchannels.config.AppConstants;
import com.ftd.services.fulfillmentchannels.dao.CalendarAPIDao;
import com.ftd.services.fulfillmentchannels.domain.api.request.CalendarAPIRequest;
import com.ftd.services.fulfillmentchannels.domain.api.response.DeliveryAPIResponse;
import com.ftd.services.fulfillmentchannels.domain.internal.enums.SiteId;
import com.ftd.services.fulfillmentchannels.entity.FulfillmentChannelsEntity;
import com.ftd.services.fulfillmentchannels.entity.ProductGroup;
import com.ftd.services.fulfillmentchannels.service.CalendarAPIService;
import edu.emory.mathcs.backport.java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

@Slf4j
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Service
public class CalendarAPIServiceImpl extends BaseAPIServiceImpl implements CalendarAPIService {

    @Autowired
    private CalendarAPIDao dao;

    private ExecutorService service = Executors.newFixedThreadPool(AppConstants.EXE_SERV_BATCH_SIZE);

    @Override
    public Map<LocalDate, List<DeliveryAPIResponse>> getCalendarAPIFulfillmentChannels(SiteId siteId,
            CalendarAPIRequest req) {
        log.info("Invoked FulfillmentChannelServiceSubbuServiceImpl::getFulfillmentChannels");
        final LocalDateTime orderingTime = LocalDateTime.now().atZone(ZoneId.systemDefault()).toLocalDateTime();
        Optional<ProductGroup> productGroups = dao.getProductGroup(siteId, req.getProductIds());
        if (!productGroups.isPresent()) {
            throw new NoRecordsFoundException(null, null, HttpStatus.NOT_FOUND);
        } else {
            req.setProductGroup(productGroups.get().getGroupName());
        }

        Optional<List<FulfillmentChannelsEntity>> channelsOpt = dao.getCalendarAPIFulfillmentChannels(siteId, req);

        long diffInDays = Period.between(orderingTime.toLocalDate(), req.getStartDeliveryDateTime().toLocalDate())
                .getDays();
        if (diffInDays < 0) {
            req.setStartDeliveryDateTime(orderingTime);
        }
        if (channelsOpt.isPresent()) {
            Map<String, List<DeliveryAPIResponse>> dayOfWeekList = new HashMap<>();
            List<FulfillmentChannelsEntity> channels = channelsOpt.get();
            for (int k = 0; k < AppConstants.DAYS.length; k++) {
                Entry<String, List<DeliveryAPIResponse>> x = this.getDayCalendarAvailability(channels,
                        AppConstants.DAYS[k], productGroups.get(), orderingTime);
                dayOfWeekList.put(x.getKey(), x.getValue());
            }
            LocalDate delStartDate = req.getStartDeliveryDateTime().toLocalDate();
            LocalDate delEndDate = req.getEndDeliveryDateTime().toLocalDate();
            Map<LocalDate, List<DeliveryAPIResponse>> finalOut = new HashMap<>();
            for (LocalDate delDate = delStartDate; delDate.isBefore(delEndDate)
                    || delDate.isEqual(delEndDate); delDate = delDate.plusDays(1)) {
                finalOut.put(delDate, dayOfWeekList.get(delDate.getDayOfWeek()
                        .getDisplayName(TextStyle.SHORT_STANDALONE, Locale.getDefault()).toLowerCase()));
            }
            return finalOut;
        } else {
            throw new NoRecordsFoundException("RESOURCE_NOT_FOUND",
                    "No records found for the given site id - " + siteId, HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public Map<LocalDate, List<DeliveryAPIResponse>> getCalendarAPIFulfillmentChannelsRx(SiteId siteId,
            CalendarAPIRequest req) {
        log.info("Invoked FulfillmentChannelServiceSubbuServiceImpl::getFulfillmentChannels");
        final LocalDateTime orderingTime = LocalDateTime.now().atZone(ZoneId.systemDefault()).toLocalDateTime();
        Optional<ProductGroup> productGroups = dao.getProductGroup(siteId, req.getProductIds());
        if (!productGroups.isPresent()) {
            throw new NoRecordsFoundException(null, null, HttpStatus.NOT_FOUND);
        } else {
            req.setProductGroup(productGroups.get().getGroupName());
        }
        Optional<List<FulfillmentChannelsEntity>> channelsOpt = dao.getCalendarAPIFulfillmentChannels(siteId, req);

        long diffInDays = Period.between(orderingTime.toLocalDate(), req.getStartDeliveryDateTime().toLocalDate())
                .getDays();
        if (diffInDays < 0) {
            req.setStartDeliveryDateTime(orderingTime);
        }

        if (channelsOpt.isPresent() && !ObjectUtils.isEmpty(channelsOpt.get())) {
            Map<String, List<DeliveryAPIResponse>> dayOfWeekList = new HashMap<>();
            List<FulfillmentChannelsEntity> channels = channelsOpt.get();

            @SuppressWarnings("unchecked")
            List<String> days = Arrays.asList(AppConstants.DAYS);

            Observable.fromIterable(days)
                    .flatMap(val -> Observable.just(val).subscribeOn(Schedulers.from(getService())).map(
                            val1 -> this.getDayCalendarAvailability(channels, val1, productGroups.get(), orderingTime)))
                    .blockingSubscribe(x -> dayOfWeekList.put(x.getKey(), x.getValue()));

            LocalDate delStartDate = req.getStartDeliveryDateTime().toLocalDate();
            LocalDate delEndDate = req.getEndDeliveryDateTime().toLocalDate();
            Map<LocalDate, List<DeliveryAPIResponse>> finalOut = new HashMap<>();
            for (LocalDate delDate = delStartDate; delDate.isBefore(delEndDate)
                    || delDate.isEqual(delEndDate); delDate = delDate.plusDays(1)) {
                finalOut.put(delDate, dayOfWeekList.get(delDate.getDayOfWeek()
                        .getDisplayName(TextStyle.SHORT_STANDALONE, Locale.getDefault()).toLowerCase()));
            }
            req.setDeliveryDateTime(req.getStartDeliveryDateTime());
            finalOut.put(delStartDate,
                    this.getDeliveryAvailability(channelsOpt.get(), req, productGroups.get(), orderingTime));
            req.setDeliveryDateTime(req.getStartDeliveryDateTime().plusDays(1));
            finalOut.put(delStartDate,
                    this.getDeliveryAvailability(channelsOpt.get(), req, productGroups.get(), orderingTime));
            return finalOut;
        } else {
            throw new NoRecordsFoundException("RESOURCE_NOT_FOUND",
                    "No records found for the given site id - " + siteId, HttpStatus.NOT_FOUND);
        }

    }

}
