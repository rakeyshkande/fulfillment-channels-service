package com.ftd.services.fulfillmentchannels.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import com.ftd.services.fulfillmentchannels.domain.api.request.CalendarAPIRequest;
import com.ftd.services.fulfillmentchannels.domain.api.response.DeliveryAPIResponse;
import com.ftd.services.fulfillmentchannels.domain.internal.enums.SiteId;

public interface CalendarAPIService {

    Map<LocalDate, List<DeliveryAPIResponse>> getCalendarAPIFulfillmentChannels(SiteId siteId, CalendarAPIRequest req);

    Map<LocalDate, List<DeliveryAPIResponse>> getCalendarAPIFulfillmentChannelsRx(SiteId siteId,
            CalendarAPIRequest req);

}
