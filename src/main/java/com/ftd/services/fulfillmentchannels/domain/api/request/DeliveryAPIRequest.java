package com.ftd.services.fulfillmentchannels.domain.api.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.ftd.services.fulfillmentchannels.domain.internal.enums.SiteId;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeliveryAPIRequest implements Serializable {

    private static final long serialVersionUID = -3085141664655889095L;

    @ApiModelProperty(hidden = true)
    private SiteId siteId;

    @ApiModelProperty(hidden = true)
    private String productGroup;

    @ApiModelProperty(required = true, value =
            "The fulfillment date in format yyyy-MM-dd'T'HH:mm:ss to filter channels by.")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private transient LocalDateTime deliveryDateTime;

    @ApiModelProperty(required = true, value = "The product ids to filter channels by.")
    private List<String> productIds;

    @ApiModelProperty(value = "The delivery zip code to filter channels by.")
    private String deliveryZipCode;

    @Override
    public String toString() {
        return "FulfillmentChannelsRequest{" +
                "deliveryDateTime=" + deliveryDateTime +
                ", productIds=" + productIds +
                ", deliveryZipCode='" + deliveryZipCode + '\'' +
                '}';
    }
}
