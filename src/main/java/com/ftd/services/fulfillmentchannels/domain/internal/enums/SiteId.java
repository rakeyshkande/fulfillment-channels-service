package com.ftd.services.fulfillmentchannels.domain.internal.enums;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonValue;

public enum SiteId {

    FTD("FTD"), PROFLOWERS("PROFLOWERS");

    private final String value;
    private static final Map<String, SiteId> CONSTANTS = new HashMap<>();

    SiteId(String value) {
        this.value = value;
    }

    @JsonValue
    public String value() {
        return this.value;
    }

    static {
        for (SiteId c : values()) {
            CONSTANTS.put(c.value, c);
        }
    }

    public static SiteId lookup(String siteId) {
        SiteId constant = CONSTANTS.get(siteId);
        if (constant == null) {
            throw new IllegalArgumentException(
                    "Unknown enum type " + siteId + ", Allowed values are " + Arrays.toString(values()));
        } else {
            return constant;
        }
    }
}