package com.ftd.services.fulfillmentchannels.domain.internal;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ftd.services.fulfillmentchannels.domain.api.response.DeliveryAPIResponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CalendarAPITempOut {

    private String dayOfWeek;
    private List<String> options;
    private List<DeliveryAPIResponse> fulfilmentOptions;
}
