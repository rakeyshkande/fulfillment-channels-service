package com.ftd.services.fulfillmentchannels.domain.api.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CalendarAPIRequest extends DeliveryAPIRequest {

    private static final long serialVersionUID = -1274074990662619968L;

    @ApiModelProperty(required = true,
            value = "The fulfillment date in format yyyy-MM-dd'T'HH:mm:ss to filter channels by.")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private transient LocalDateTime startDeliveryDateTime;

    @ApiModelProperty(required = true,
            value = "The fulfillment date in format yyyy-MM-dd'T'HH:mm:ss to filter channels by.")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private transient LocalDateTime endDeliveryDateTime;

    @Override
    public String toString() {
        return "CalendarAPIRequest [startDeliveryDateTime=" + startDeliveryDateTime + ", endDeliveryDateTime="
                + endDeliveryDateTime + ", toString()=" + super.toString() + "]";
    }

}
