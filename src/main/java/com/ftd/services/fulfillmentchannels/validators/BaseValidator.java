package com.ftd.services.fulfillmentchannels.validators;

import org.springframework.util.ObjectUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

interface DataValidator {
    boolean validate(String data);
}

public class BaseValidator {

    private static Map<String, DataValidator> map = new HashMap<>();

    private static void init() {
        DataValidator nonemptyValidator = data -> !(ObjectUtils.isEmpty(data));
        map.put("nonempty", nonemptyValidator);

        DataValidator alphabeticValidator = data -> Pattern.matches("^[A-Za-z_ ]+$", data);
        map.put("alphabetic", alphabeticValidator);

        DataValidator alphanumericValidator = data -> Pattern.matches("^[A-Za-z0-9 \\.]+$", data);
        map.put("alphanumeric", alphanumericValidator);

        DataValidator alphabeticListValidator = data -> Pattern.matches("^(?:[a-zA-Z ]+,)*[a-zA-Z ]+$", data);
        map.put("alphabeticList", alphabeticListValidator);

        DataValidator alphanumericListValidator = data -> Pattern.matches("^(?:[a-zA-Z0-9 ]+,)*[a-zA-Z0-9 ]+$", data);
        map.put("alphanumericList", alphanumericListValidator);

        DataValidator integerValidator = data -> Pattern.matches("^[-]?\\d+$", data);
        map.put("integer", integerValidator);

        DataValidator integerListValidator = data -> Pattern.matches("^\\d+(,\\d+)*$", data);
        map.put("integerList", integerListValidator);

        DataValidator numericValidator = data -> Pattern.matches("^\\s*[-+]?\\d*\\.?\\d+$", data);
        map.put("numeric", numericValidator);

        DataValidator numericListValidator = data -> Pattern
                .matches("^(\\s*-?\\d+(\\.\\d+)?)(\\s*,\\s*-?\\d+(\\.\\d+)?)*$", data);
        map.put("numericList", numericListValidator);

        DataValidator booleanValidator = data -> Pattern.matches("^([Tt][Rr][Uu][Ee]|[Ff][Aa][Ll][Ss][Ee])$", data);
        map.put("boolean", booleanValidator);

        DataValidator emailValidator = data -> Pattern
                .matches("\\b^['_a-zA-Z0-9-\\+]+(\\.['_a-zA-Z0-9-\\+]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\."
                        + "([A-Za-z0-9]{1,})$\\b", data);
        map.put("email", emailValidator);

        DataValidator emailListValidator = data -> Pattern
                .matches("((?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|�"
                        + "(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\"
                        + "[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*�)@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)"
                        + "+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.)"
                        + "{3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c"
                        + "\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])+,)*"
                        + "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|�(?:[\\x01-\\x08\\x0b"
                        + "\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*�)"
                        + "@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25"
                        + "[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*"
                        + "[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\"
                        + "x0b\\x0c\\x0e-\\x7f])+)\\])+$", data);
        map.put("emailList", emailListValidator);

        DataValidator phonenumberValidator = data -> Pattern.matches("^(?:[0-9 ]+,)*[0-9 ]+$", data);
        map.put("phonenumber", phonenumberValidator);

        DataValidator weekdayValidator = data -> Pattern.matches("[0-6]", data);
        map.put("weekdaynumber", weekdayValidator);

        DataValidator weekdayListValidator = data -> Pattern.matches("^[0-6](,[0-6])*$", data);
        map.put("weekdaynumberList", weekdayListValidator);

        DataValidator datenumberValidator = data -> Pattern.matches("^(([0]?[1-9])|([1-2][0-9])|(3[01]))$", data);
        map.put("datenumber", datenumberValidator);

        DataValidator hourValidator = data -> Pattern.matches("^(([0]?[1-9])|([1][0-9])|(2[0-4]))$", data);
        map.put("hour", hourValidator);

        DataValidator monthValidator = data -> Pattern.matches("^(([0]?[1-9])|([1][0-2]))$", data);
        map.put("monthnumber", monthValidator);

        DataValidator yearValidator = data -> Pattern.matches("^\\d{4}$", data);
        map.put("year", yearValidator);

        DataValidator urlValidator = data -> Pattern
                .matches("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]", data);
        map.put("url", urlValidator);

        DataValidator filepathValidator = data -> Pattern.matches("^(?:\\.{2})?(?:\\/\\.{2})*(\\/[a-zA-Z0-9]+)+$",
                data);
        map.put("filepath", filepathValidator);

    }

    public static DataValidator getValidator(String validatorType) {
        if (map.isEmpty()) {
            init();
        }
        return map.get(validatorType);
    }
}
