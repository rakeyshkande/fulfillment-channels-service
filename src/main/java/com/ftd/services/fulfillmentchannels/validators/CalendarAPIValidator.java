package com.ftd.services.fulfillmentchannels.validators;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import com.ftd.commons.misc.exception.InputValidationException;
import com.ftd.services.fulfillmentchannels.domain.api.request.CalendarAPIRequest;
import com.ftd.services.fulfillmentchannels.domain.internal.enums.SiteId;

@Component
public class CalendarAPIValidator extends BaseValidator {

    public void validateRequestBody(SiteId siteId, CalendarAPIRequest req) {

        if (!siteId.value().equals(req.getSiteId().value())) {
            throw new InputValidationException(HttpStatus.UNPROCESSABLE_ENTITY.name(), "INVALID_INPUT");
        }
    }

}
