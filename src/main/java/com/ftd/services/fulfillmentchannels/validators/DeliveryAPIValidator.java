package com.ftd.services.fulfillmentchannels.validators;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import com.ftd.commons.misc.exception.InputValidationException;
import com.ftd.services.fulfillmentchannels.domain.api.request.DeliveryAPIRequest;
import com.ftd.services.fulfillmentchannels.domain.internal.enums.SiteId;

@Component
public class DeliveryAPIValidator extends BaseValidator {

    public void validateRequestBody(SiteId siteId, DeliveryAPIRequest req) {

        if (!siteId.value().equals(req.getSiteId().value())) {
            throw new InputValidationException(HttpStatus.UNPROCESSABLE_ENTITY.name(), "INVALID_INPUT");
        }
    }

}
