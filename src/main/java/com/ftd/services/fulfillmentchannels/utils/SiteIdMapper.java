package com.ftd.services.fulfillmentchannels.utils;

import java.beans.PropertyEditorSupport;

import com.ftd.services.fulfillmentchannels.domain.internal.enums.SiteId;

public class SiteIdMapper extends PropertyEditorSupport {

    @Override
    public void setAsText(final String site) {
        setValue(SiteId.lookup(site));
    }
}