package com.ftd.services.fulfillmentchannels.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.CacheControl;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftd.services.fulfillmentchannels.domain.api.request.CalendarAPIRequest;
import com.ftd.services.fulfillmentchannels.domain.api.response.DeliveryAPIResponse;
import com.ftd.services.fulfillmentchannels.domain.internal.enums.SiteId;
import com.ftd.services.fulfillmentchannels.service.CalendarAPIService;
import com.ftd.services.fulfillmentchannels.validators.CalendarAPIValidator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@NoArgsConstructor
@AllArgsConstructor
@RestController
@RefreshScope
@RequestMapping(produces = "application/hal+json")
public class CalendarAPIController {

    @Autowired
    private CalendarAPIService service;
    @Autowired
    private CalendarAPIValidator validator;

    @GetMapping(value = "/{siteId}/fulfilment-channels/calendar-api", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<LocalDate, List<DeliveryAPIResponse>>> getCalendarAvailability(
            @PathVariable(name = "siteId", required = true) final SiteId siteId, CalendarAPIRequest req) {
        log.info("Get fulfillment channels by request-{}", req);
        validator.validateRequestBody(siteId, req);
        return ResponseEntity.ok().cacheControl(CacheControl.noCache())
                .body(this.getService().getCalendarAPIFulfillmentChannels(siteId, req));
    }

    @GetMapping(value = "/{siteId}/fulfilment-channels/calendar-api-rx", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<LocalDate, List<DeliveryAPIResponse>>> getCalendarAvailabilityRx(
            @PathVariable(name = "siteId", required = true) final SiteId siteId, CalendarAPIRequest req) {
        log.info("Get fulfillment channels rx by request-{}", req);
        validator.validateRequestBody(siteId, req);
        return ResponseEntity.ok().cacheControl(CacheControl.noCache())
                .body(this.getService().getCalendarAPIFulfillmentChannelsRx(siteId, req));
    }
}
