package com.ftd.services.fulfillmentchannels.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.CacheControl;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftd.services.fulfillmentchannels.domain.api.request.DeliveryAPIRequest;
import com.ftd.services.fulfillmentchannels.domain.api.response.DeliveryAPIResponse;
import com.ftd.services.fulfillmentchannels.domain.internal.enums.SiteId;
import com.ftd.services.fulfillmentchannels.service.DeliveryAPIService;
import com.ftd.services.fulfillmentchannels.validators.DeliveryAPIValidator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@NoArgsConstructor
@AllArgsConstructor
@RestController
@RefreshScope
@RequestMapping(produces = "application/hal+json")
public class DeliveryAPIController {

    @Autowired
    private DeliveryAPIService service;
    @Autowired
    private DeliveryAPIValidator validator;

    @GetMapping(value = "/{siteId}/fulfilment-channels-java/delivery-api", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DeliveryAPIResponse>> getDeliveryAPIFulfillmentChannels(
            @PathVariable(name = "siteId", required = true) final SiteId siteId, DeliveryAPIRequest req) {
        log.info("Get fulfillment channels by request-{}", req);
        validator.validateRequestBody(siteId, req);
        return ResponseEntity.ok().cacheControl(CacheControl.noCache())
                .body(this.getService().getDeliveryAPIFulfillmentChannels(siteId, req));
    }

}
