package com.ftd.services.fulfillmentchannels;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FulfillmentChannelsServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(FulfillmentChannelsServiceApplication.class, args);
    }

}
