package com.ftd.services.fulfillmentchannels.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.ftd.services.fulfillmentchannels.domain.internal.enums.SiteId;
import com.ftd.services.fulfillmentchannels.entity.ProductGroup;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class BaseAPIDaoImpl {

    @Autowired
    private MongoTemplate template;

    public MongoTemplate getTemplate() {
        return template;
    }

    public void setTemplate(MongoTemplate template) {
        this.template = template;
    }

    public Optional<ProductGroup> getProductGroup(SiteId siteId, List<String> productCodes) {
        log.info("in DAO");
        Query filter = Query.query(Criteria.where("products.code").in(productCodes).and("siteId").is(siteId.value()));
        return Optional.ofNullable(this.getTemplate().findOne(filter, ProductGroup.class));
    }
}
