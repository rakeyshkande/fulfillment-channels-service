package com.ftd.services.fulfillmentchannels.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.ftd.services.fulfillmentchannels.dao.CalendarAPIDao;
import com.ftd.services.fulfillmentchannels.domain.api.request.CalendarAPIRequest;
import com.ftd.services.fulfillmentchannels.domain.internal.enums.SiteId;
import com.ftd.services.fulfillmentchannels.entity.FulfillmentChannelsEntity;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class CalendarAPIDaoImpl extends BaseAPIDaoImpl implements CalendarAPIDao {

    @Override
    public Optional<List<FulfillmentChannelsEntity>> getCalendarAPIFulfillmentChannels(SiteId siteId,
            CalendarAPIRequest req) {
        log.info("in DAO");
        Query filter = Query.query(Criteria.where("deliveryZipCode").is(req.getDeliveryZipCode()).and("productGroups")
                .is(req.getProductGroup()).and("siteGroup").is("ftd"));
        return Optional.ofNullable(this.getTemplate().find(filter, FulfillmentChannelsEntity.class));
    }

}
