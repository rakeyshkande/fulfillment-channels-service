package com.ftd.services.fulfillmentchannels.dao;

import java.util.List;
import java.util.Optional;

import com.ftd.services.fulfillmentchannels.domain.api.request.DeliveryAPIRequest;
import com.ftd.services.fulfillmentchannels.domain.internal.enums.SiteId;
import com.ftd.services.fulfillmentchannels.entity.FulfillmentChannelsEntity;

public interface DeliveryAPIDao extends BaseAPIDao {

    Optional<List<FulfillmentChannelsEntity>> getDeliveryFulfillmentChannels(SiteId siteId, DeliveryAPIRequest req);

}
