package com.ftd.services.fulfillmentchannels.dao;

import java.util.List;
import java.util.Optional;

import com.ftd.services.fulfillmentchannels.domain.internal.enums.SiteId;
import com.ftd.services.fulfillmentchannels.entity.ProductGroup;

public interface BaseAPIDao {

    Optional<ProductGroup> getProductGroup(SiteId siteId, List<String> productIds);

}
