package com.ftd.services.fulfillmentchannels.dao;

import java.util.List;
import java.util.Optional;

import com.ftd.services.fulfillmentchannels.domain.api.request.CalendarAPIRequest;
import com.ftd.services.fulfillmentchannels.domain.internal.enums.SiteId;
import com.ftd.services.fulfillmentchannels.entity.FulfillmentChannelsEntity;

public interface CalendarAPIDao extends BaseAPIDao {

    Optional<List<FulfillmentChannelsEntity>> getCalendarAPIFulfillmentChannels(SiteId siteId, CalendarAPIRequest req);

}
